//
//  AppDelegate.h
//  WeatherApp
//
//  Created by Luigi Mazzarella on 08/09/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@end

