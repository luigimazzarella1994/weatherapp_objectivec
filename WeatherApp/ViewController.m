//
//  ViewController.m
//  WeatherApp
//
//  Created by Luigi Mazzarella on 08/09/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()


@property NSString *urlAPi;
- (IBAction)ViewMeteo:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *cityTex;
@property (weak, nonatomic) IBOutlet UILabel *tempFeelsLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempMaxLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempMinLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *weatherConditionLable;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *hello = @"Hello world!";
    NSLog(@"%@", hello);
   
}




- (IBAction)ViewMeteo:(UIButton *)sender {
    
    NSString *city = _cityTex.text;
    NSString *iconName;
    
    _urlAPi = [NSString stringWithFormat:@"https://api.openweathermap.org/data/2.5/weather?q=%@&APPID=ffa86372f2df0072c7af1aaf54ce5fcc&units=metric&lang=it",city];
    
    NSError  *error;
    NSURLResponse *response;
    
    NSData *data = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_urlAPi ]] returningResponse:&response error:&error];
    
    if (data) {
        NSData *jsonData = [NSJSONSerialization JSONObjectWithData:data options: kNilOptions error:&error];
        NSLog(@"%@", jsonData);
        
        NSObject *main = [jsonData valueForKey:@"main"];
       _tempFeelsLabel.text =[[NSString alloc] initWithFormat:@"%@", [main valueForKey:@"feels_like"]];
        _tempMinLabel.text =  [[NSString alloc] initWithFormat:@"%@", [main valueForKey:@"temp_min"]];
        _tempMaxLabel.text = [[NSString alloc] initWithFormat:@"%@", [main valueForKey:@"temp_max"]];
        NSArray *weather = [jsonData valueForKey:@"weather"];
        NSObject *weatherContent = weather[0];
        iconName = [weatherContent valueForKey:@"icon"];
        _weatherConditionLable.text =[weatherContent valueForKey:@"description"];
        
        NSLog(@"IconName %@", iconName);
        
        NSString *urlImage =[NSString stringWithFormat:@"https://openweathermap.org/img/wn/%@@2x.png",iconName];

        
        NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: urlImage]];
        _iconImage.image = [UIImage imageWithData: imageData];
        
    }
    else {
        NSLog(@"%@", error);
    }
    
    
    
}
@end
