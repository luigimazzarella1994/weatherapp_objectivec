//
//  SceneDelegate.h
//  WeatherApp
//
//  Created by Luigi Mazzarella on 08/09/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

